#![allow(clippy::needless_doctest_main)]
//! # Rust Shumate bindings
//!
//! This library contains safe Rust bindings for [Shumate](https://gitlab.gnome.org/GNOME/libshumate), a library that offers
//! a GTK4 widget to display maps.
//!
//! See also
//!
//! - [GTK 4 Rust bindings documentation](mod@gtk)
//! - [libshumate documentation](https://gnome.pages.gitlab.gnome.org/libshumate/)
//! - [gtk-rs project overview](https://gtk-rs.org/)
//!
//! # Example
//!
//! The [`libshumate`](mod@crate) crate is usually renamed to `shumate`. You can
//! do this globally in your `Cargo.toml` file:
//!
//! ```toml
//! [dependencies.shumate]
//! package = "libshumate"
//! version = "0.x.y"
//! ```
//!
//! ```no_run
//! # use libshumate as shumate;
//! use shumate::prelude::*;
//!
//! use shumate::{Map};
//! use gtk::{Application, Box, ListBox, Orientation, ApplicationWindow};
//!
//! fn main() {
//!     let application = Application::builder()
//!         .application_id("com.example.FirstShumateApp")
//!         .build();
//!
//!     application.connect_activate(|app| {
//!         let content = Map::new();
//!
//!         let window = ApplicationWindow::builder()
//!             .application(app)
//!             .default_width(350)
//!             // add content to window
//!             .child(&content)
//!             .build();
//!         window.show();
//!     });
//!
//!     application.run();
//! }
//! ```

// Re-export the -sys bindings
pub use ffi;
#[doc(hidden)]
pub use gtk;

#[doc(alias = "SHUMATE_MAX_LATITUDE")]
pub const MAX_LATITUDE: f64 = ffi::SHUMATE_MAX_LATITUDE;
#[doc(alias = "SHUMATE_MAX_LONGITUDE")]
pub const MAX_LONGITUDE: f64 = ffi::SHUMATE_MAX_LONGITUDE;
#[doc(alias = "SHUMATE_MIN_LATITUDE")]
pub const MIN_LATITUDE: f64 = ffi::SHUMATE_MIN_LATITUDE;
#[doc(alias = "SHUMATE_MIN_LONGITUDE")]
pub const MIN_LONGITUDE: f64 = ffi::SHUMATE_MIN_LONGITUDE;

/// Asserts that this is the main thread and `gtk::init` has been called.
macro_rules! assert_initialized_main_thread {
    () => {
        if !::gtk::is_initialized_main_thread() {
            if ::gtk::is_initialized() {
                panic!("libshumate may only be used from the main thread.");
            } else {
                panic!("Gtk has to be initialized before using libshumate.");
            }
        }
    };
}

macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(unused_imports)]
#[allow(clippy::type_complexity)]
#[allow(clippy::too_many_arguments)]
mod auto;

pub use auto::*;

pub mod prelude;
pub mod subclass;

pub mod functions {
    pub use super::auto::functions::*;
}
