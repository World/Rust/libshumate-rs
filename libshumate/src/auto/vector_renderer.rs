// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT
#![allow(deprecated)]

#[cfg(feature = "v1_2")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_2")))]
use crate::DataSource;
#[cfg(feature = "v1_1")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_1")))]
use crate::VectorSpriteSheet;
use crate::{ffi, MapProjection, MapSource};
#[cfg(feature = "v1_1")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_1")))]
use glib::signal::{connect_raw, SignalHandlerId};
use glib::{prelude::*, translate::*};
#[cfg(feature = "v1_1")]
#[cfg_attr(docsrs, doc(cfg(feature = "v1_1")))]
use std::boxed::Box as Box_;

glib::wrapper! {
    #[doc(alias = "ShumateVectorRenderer")]
    pub struct VectorRenderer(Object<ffi::ShumateVectorRenderer, ffi::ShumateVectorRendererClass>) @extends MapSource, @implements gio::Initable;

    match fn {
        type_ => || ffi::shumate_vector_renderer_get_type(),
    }
}

impl VectorRenderer {
    #[doc(alias = "shumate_vector_renderer_new")]
    pub fn new(id: &str, style_json: &str) -> Result<VectorRenderer, glib::Error> {
        assert_initialized_main_thread!();
        unsafe {
            let mut error = std::ptr::null_mut();
            let ret = ffi::shumate_vector_renderer_new(
                id.to_glib_none().0,
                style_json.to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    // rustdoc-stripper-ignore-next
    /// Creates a new builder-pattern struct instance to construct [`VectorRenderer`] objects.
    ///
    /// This method returns an instance of [`VectorRendererBuilder`](crate::builders::VectorRendererBuilder) which can be used to create [`VectorRenderer`] objects.
    pub fn builder() -> VectorRendererBuilder {
        VectorRendererBuilder::new()
    }

    #[cfg(feature = "v1_1")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_1")))]
    #[doc(alias = "shumate_vector_renderer_get_sprite_sheet")]
    #[doc(alias = "get_sprite_sheet")]
    #[doc(alias = "sprite-sheet")]
    pub fn sprite_sheet(&self) -> Option<VectorSpriteSheet> {
        unsafe {
            from_glib_none(ffi::shumate_vector_renderer_get_sprite_sheet(
                self.to_glib_none().0,
            ))
        }
    }

    #[cfg(feature = "v1_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_2")))]
    #[doc(alias = "shumate_vector_renderer_set_data_source")]
    pub fn set_data_source(&self, name: &str, data_source: &impl IsA<DataSource>) {
        unsafe {
            ffi::shumate_vector_renderer_set_data_source(
                self.to_glib_none().0,
                name.to_glib_none().0,
                data_source.as_ref().to_glib_none().0,
            );
        }
    }

    #[cfg(feature = "v1_1")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_1")))]
    #[doc(alias = "shumate_vector_renderer_set_sprite_sheet")]
    #[doc(alias = "sprite-sheet")]
    pub fn set_sprite_sheet(&self, sprites: &VectorSpriteSheet) {
        unsafe {
            ffi::shumate_vector_renderer_set_sprite_sheet(
                self.to_glib_none().0,
                sprites.to_glib_none().0,
            );
        }
    }

    #[cfg_attr(feature = "v1_1", deprecated = "Since 1.1")]
    #[allow(deprecated)]
    #[doc(alias = "shumate_vector_renderer_set_sprite_sheet_data")]
    pub fn set_sprite_sheet_data(
        &self,
        sprites_pixbuf: &gdk_pixbuf::Pixbuf,
        sprites_json: &str,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = std::ptr::null_mut();
            let is_ok = ffi::shumate_vector_renderer_set_sprite_sheet_data(
                self.to_glib_none().0,
                sprites_pixbuf.to_glib_none().0,
                sprites_json.to_glib_none().0,
                &mut error,
            );
            debug_assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[doc(alias = "style-json")]
    pub fn style_json(&self) -> Option<glib::GString> {
        ObjectExt::property(self, "style-json")
    }

    #[doc(alias = "shumate_vector_renderer_is_supported")]
    pub fn is_supported() -> bool {
        assert_initialized_main_thread!();
        unsafe { from_glib(ffi::shumate_vector_renderer_is_supported()) }
    }

    #[cfg(feature = "v1_1")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_1")))]
    #[doc(alias = "sprite-sheet")]
    pub fn connect_sprite_sheet_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_sprite_sheet_trampoline<F: Fn(&VectorRenderer) + 'static>(
            this: *mut ffi::ShumateVectorRenderer,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::sprite-sheet\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_sprite_sheet_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl Default for VectorRenderer {
    fn default() -> Self {
        glib::object::Object::new::<Self>()
    }
}

// rustdoc-stripper-ignore-next
/// A [builder-pattern] type to construct [`VectorRenderer`] objects.
///
/// [builder-pattern]: https://doc.rust-lang.org/1.0.0/style/ownership/builders.html
#[must_use = "The builder must be built to be used"]
pub struct VectorRendererBuilder {
    builder: glib::object::ObjectBuilder<'static, VectorRenderer>,
}

impl VectorRendererBuilder {
    fn new() -> Self {
        Self {
            builder: glib::object::Object::builder(),
        }
    }

    #[cfg(feature = "v1_1")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_1")))]
    pub fn sprite_sheet(self, sprite_sheet: &VectorSpriteSheet) -> Self {
        Self {
            builder: self.builder.property("sprite-sheet", sprite_sheet.clone()),
        }
    }

    pub fn style_json(self, style_json: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("style-json", style_json.into()),
        }
    }

    pub fn id(self, id: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("id", id.into()),
        }
    }

    pub fn license(self, license: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("license", license.into()),
        }
    }

    pub fn license_uri(self, license_uri: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("license-uri", license_uri.into()),
        }
    }

    pub fn max_zoom_level(self, max_zoom_level: u32) -> Self {
        Self {
            builder: self.builder.property("max-zoom-level", max_zoom_level),
        }
    }

    pub fn min_zoom_level(self, min_zoom_level: u32) -> Self {
        Self {
            builder: self.builder.property("min-zoom-level", min_zoom_level),
        }
    }

    pub fn name(self, name: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("name", name.into()),
        }
    }

    pub fn projection(self, projection: MapProjection) -> Self {
        Self {
            builder: self.builder.property("projection", projection),
        }
    }

    pub fn tile_size(self, tile_size: u32) -> Self {
        Self {
            builder: self.builder.property("tile-size", tile_size),
        }
    }

    // rustdoc-stripper-ignore-next
    /// Build the [`VectorRenderer`].
    #[must_use = "Building the object from the builder is usually expensive and is not expected to have side effects"]
    pub fn build(self) -> VectorRenderer {
        self.builder.build()
    }
}
