// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::{ffi, Location, MapSource};
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::boxed::Box as Box_;

glib::wrapper! {
    #[doc(alias = "ShumateViewport")]
    pub struct Viewport(Object<ffi::ShumateViewport, ffi::ShumateViewportClass>) @implements Location;

    match fn {
        type_ => || ffi::shumate_viewport_get_type(),
    }
}

impl Viewport {
    #[doc(alias = "shumate_viewport_new")]
    pub fn new() -> Viewport {
        assert_initialized_main_thread!();
        unsafe { from_glib_full(ffi::shumate_viewport_new()) }
    }

    // rustdoc-stripper-ignore-next
    /// Creates a new builder-pattern struct instance to construct [`Viewport`] objects.
    ///
    /// This method returns an instance of [`ViewportBuilder`](crate::builders::ViewportBuilder) which can be used to create [`Viewport`] objects.
    pub fn builder() -> ViewportBuilder {
        ViewportBuilder::new()
    }

    #[doc(alias = "shumate_viewport_get_max_zoom_level")]
    #[doc(alias = "get_max_zoom_level")]
    #[doc(alias = "max-zoom-level")]
    pub fn max_zoom_level(&self) -> u32 {
        unsafe { ffi::shumate_viewport_get_max_zoom_level(self.to_glib_none().0) }
    }

    #[doc(alias = "shumate_viewport_get_min_zoom_level")]
    #[doc(alias = "get_min_zoom_level")]
    #[doc(alias = "min-zoom-level")]
    pub fn min_zoom_level(&self) -> u32 {
        unsafe { ffi::shumate_viewport_get_min_zoom_level(self.to_glib_none().0) }
    }

    #[doc(alias = "shumate_viewport_get_reference_map_source")]
    #[doc(alias = "get_reference_map_source")]
    #[doc(alias = "reference-map-source")]
    pub fn reference_map_source(&self) -> Option<MapSource> {
        unsafe {
            from_glib_none(ffi::shumate_viewport_get_reference_map_source(
                self.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "shumate_viewport_get_rotation")]
    #[doc(alias = "get_rotation")]
    pub fn rotation(&self) -> f64 {
        unsafe { ffi::shumate_viewport_get_rotation(self.to_glib_none().0) }
    }

    #[doc(alias = "shumate_viewport_get_zoom_level")]
    #[doc(alias = "get_zoom_level")]
    #[doc(alias = "zoom-level")]
    pub fn zoom_level(&self) -> f64 {
        unsafe { ffi::shumate_viewport_get_zoom_level(self.to_glib_none().0) }
    }

    #[doc(alias = "shumate_viewport_location_to_widget_coords")]
    pub fn location_to_widget_coords(
        &self,
        widget: &impl IsA<gtk::Widget>,
        latitude: f64,
        longitude: f64,
    ) -> (f64, f64) {
        unsafe {
            let mut x = std::mem::MaybeUninit::uninit();
            let mut y = std::mem::MaybeUninit::uninit();
            ffi::shumate_viewport_location_to_widget_coords(
                self.to_glib_none().0,
                widget.as_ref().to_glib_none().0,
                latitude,
                longitude,
                x.as_mut_ptr(),
                y.as_mut_ptr(),
            );
            (x.assume_init(), y.assume_init())
        }
    }

    #[doc(alias = "shumate_viewport_set_max_zoom_level")]
    #[doc(alias = "max-zoom-level")]
    pub fn set_max_zoom_level(&self, max_zoom_level: u32) {
        unsafe {
            ffi::shumate_viewport_set_max_zoom_level(self.to_glib_none().0, max_zoom_level);
        }
    }

    #[doc(alias = "shumate_viewport_set_min_zoom_level")]
    #[doc(alias = "min-zoom-level")]
    pub fn set_min_zoom_level(&self, min_zoom_level: u32) {
        unsafe {
            ffi::shumate_viewport_set_min_zoom_level(self.to_glib_none().0, min_zoom_level);
        }
    }

    #[doc(alias = "shumate_viewport_set_reference_map_source")]
    #[doc(alias = "reference-map-source")]
    pub fn set_reference_map_source(&self, map_source: Option<&impl IsA<MapSource>>) {
        unsafe {
            ffi::shumate_viewport_set_reference_map_source(
                self.to_glib_none().0,
                map_source.map(|p| p.as_ref()).to_glib_none().0,
            );
        }
    }

    #[doc(alias = "shumate_viewport_set_rotation")]
    #[doc(alias = "rotation")]
    pub fn set_rotation(&self, rotation: f64) {
        unsafe {
            ffi::shumate_viewport_set_rotation(self.to_glib_none().0, rotation);
        }
    }

    #[doc(alias = "shumate_viewport_set_zoom_level")]
    #[doc(alias = "zoom-level")]
    pub fn set_zoom_level(&self, zoom_level: f64) {
        unsafe {
            ffi::shumate_viewport_set_zoom_level(self.to_glib_none().0, zoom_level);
        }
    }

    #[doc(alias = "shumate_viewport_widget_coords_to_location")]
    pub fn widget_coords_to_location(
        &self,
        widget: &impl IsA<gtk::Widget>,
        x: f64,
        y: f64,
    ) -> (f64, f64) {
        unsafe {
            let mut latitude = std::mem::MaybeUninit::uninit();
            let mut longitude = std::mem::MaybeUninit::uninit();
            ffi::shumate_viewport_widget_coords_to_location(
                self.to_glib_none().0,
                widget.as_ref().to_glib_none().0,
                x,
                y,
                latitude.as_mut_ptr(),
                longitude.as_mut_ptr(),
            );
            (latitude.assume_init(), longitude.assume_init())
        }
    }

    #[doc(alias = "max-zoom-level")]
    pub fn connect_max_zoom_level_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_max_zoom_level_trampoline<F: Fn(&Viewport) + 'static>(
            this: *mut ffi::ShumateViewport,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::max-zoom-level\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_max_zoom_level_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "min-zoom-level")]
    pub fn connect_min_zoom_level_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_min_zoom_level_trampoline<F: Fn(&Viewport) + 'static>(
            this: *mut ffi::ShumateViewport,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::min-zoom-level\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_min_zoom_level_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "reference-map-source")]
    pub fn connect_reference_map_source_notify<F: Fn(&Self) + 'static>(
        &self,
        f: F,
    ) -> SignalHandlerId {
        unsafe extern "C" fn notify_reference_map_source_trampoline<F: Fn(&Viewport) + 'static>(
            this: *mut ffi::ShumateViewport,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::reference-map-source\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_reference_map_source_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "rotation")]
    pub fn connect_rotation_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_rotation_trampoline<F: Fn(&Viewport) + 'static>(
            this: *mut ffi::ShumateViewport,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::rotation\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_rotation_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "zoom-level")]
    pub fn connect_zoom_level_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_zoom_level_trampoline<F: Fn(&Viewport) + 'static>(
            this: *mut ffi::ShumateViewport,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::zoom-level\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_zoom_level_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl Default for Viewport {
    fn default() -> Self {
        Self::new()
    }
}

// rustdoc-stripper-ignore-next
/// A [builder-pattern] type to construct [`Viewport`] objects.
///
/// [builder-pattern]: https://doc.rust-lang.org/1.0.0/style/ownership/builders.html
#[must_use = "The builder must be built to be used"]
pub struct ViewportBuilder {
    builder: glib::object::ObjectBuilder<'static, Viewport>,
}

impl ViewportBuilder {
    fn new() -> Self {
        Self {
            builder: glib::object::Object::builder(),
        }
    }

    pub fn max_zoom_level(self, max_zoom_level: u32) -> Self {
        Self {
            builder: self.builder.property("max-zoom-level", max_zoom_level),
        }
    }

    pub fn min_zoom_level(self, min_zoom_level: u32) -> Self {
        Self {
            builder: self.builder.property("min-zoom-level", min_zoom_level),
        }
    }

    pub fn reference_map_source(self, reference_map_source: &impl IsA<MapSource>) -> Self {
        Self {
            builder: self.builder.property(
                "reference-map-source",
                reference_map_source.clone().upcast(),
            ),
        }
    }

    pub fn rotation(self, rotation: f64) -> Self {
        Self {
            builder: self.builder.property("rotation", rotation),
        }
    }

    pub fn zoom_level(self, zoom_level: f64) -> Self {
        Self {
            builder: self.builder.property("zoom-level", zoom_level),
        }
    }

    pub fn latitude(self, latitude: f64) -> Self {
        Self {
            builder: self.builder.property("latitude", latitude),
        }
    }

    pub fn longitude(self, longitude: f64) -> Self {
        Self {
            builder: self.builder.property("longitude", longitude),
        }
    }

    // rustdoc-stripper-ignore-next
    /// Build the [`Viewport`].
    #[must_use = "Building the object from the builder is usually expensive and is not expected to have side effects"]
    pub fn build(self) -> Viewport {
        self.builder.build()
    }
}
